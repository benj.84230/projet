package com.example.projet;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JSONResponseHandlerItemList {

    private static final String TAG = JSONResponseHandlerItemList.class.getSimpleName();


    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public List<String> readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            return readStringArray(reader);
        } finally {
            reader.close();
        }


    }

    public List<String> readStringArray(JsonReader reader) throws IOException {
        List<String> string = new ArrayList<String>();

        reader.beginArray();
        String name ;
        while (reader.hasNext()) {
            name = reader.nextString();
            string.add(name);
        }
        reader.endArray();
        return string;
    }
}

package com.example.projet;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JSONResponseHandlerItem {

    private static final String TAG = JSONResponseHandlerItem.class.getSimpleName();

    private Item item;


    public JSONResponseHandlerItem(Item item) {
        this.item = item;
    }

    public JSONResponseHandlerItem() {
        item=new Item();
    }


    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readArrayItem(reader);
        } finally {
            reader.close();
        }


    }

   /* public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }*/


    private void readArrayItem(JsonReader reader) throws IOException {


        //int nb = 0; // only consider the first element of the array
        List<String> cat = null;
        List<String> time = null;
        List<String> tech = null;
        List<String> pic = null;
        int i=0;
        reader.beginObject();

            while (reader.hasNext()) {
                String name = reader.nextName();

            if(reader.peek() == JsonToken.NULL) {

                reader.nextNull();


            }else if (name.equals("name")&& reader.peek() != JsonToken.NULL) {

                String m = reader.nextString();

                if (m != null && !m.isEmpty()){
                    item.setName(m);
                }

                } else if (name.equals("categories") && reader.peek() != JsonToken.NULL) {
                   // à def
                    cat = readStringArray(reader);
                    //String[] temp = cat.toArray(new String[cat.size()]);
                Log.d("aaazeeeeee", cat.get(0));
                    item.setCategories(cat);
                } else if (name.equals("brand")) {

                    String m = reader.nextString();
                    if (m != null && !m.isEmpty()) {

                        item.setBrand(m);
                    }
                    else{
                        reader.nextNull();
                    }

                } else if (name.equals("year")) {
                   item.setYear(reader.nextInt());
                } else if (name.equals("pictures") && reader.peek() != JsonToken.NULL) {

                    pic = readStringArrayPic(reader);
                    item.setPicture(pic);
                } else if (name.equals("timeFrame") && reader.peek() != JsonToken.NULL) {

                    time = readStringArrayInt(reader);
                Log.d("aazert", time.get(0));
                    item.setTimeFrame(time);
                } else if (name.equals("technicalDetails") && reader.peek() != JsonToken.NULL) {

                    tech = readStringArray(reader);
                    item.setTechnicalDetails(tech);
                } else if (name.equals("working")) {

                if (reader.nextBoolean()) {
                    item.setWorking(1);
                } else {
                    item.setWorking(0);
                }


                }else if (name.equals("description")){
                    item.setDescription(reader.nextString());
                } else {
                    reader.skipValue();
                }


            }



    }

    public List<String> readStringArrayInt(JsonReader reader) throws IOException {
        List<String> string = new ArrayList<String>();

        reader.beginArray();
        while (reader.hasNext()) {
            string.add(String.valueOf(reader.nextInt()));
        }
        reader.endArray();
        return string;
    }


    public List<String> readStringArray(JsonReader reader) throws IOException {
        List<String> string = new ArrayList<String>();
        int i=0;
        reader.beginArray();
        while (reader.hasNext()) {

            string.add(reader.nextString());
            Log.d("azerzeezrd", string.get(i));
            i++;
        }
        reader.endArray();
        return string;
    }

    public List<String> readStringArrayPic(JsonReader reader) throws IOException {
        List<String> string = new ArrayList<String>();

        reader.beginObject();
        while (reader.hasNext()) {
            string.add(reader.nextName());
            reader.nextString();
        }
        reader.endObject();
        return string;
    }

    public Item getItem() {
        return item;
    }
}

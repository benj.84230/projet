package com.example.projet;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }


    private static final String ITEMS_LIST = "ids";

    public static URL buildSearchTeamList() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS_LIST);

        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String SEARCH_ITEMS = "items";

    public static URL buildSearchTeam(String ItemID) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS)
                .appendPath(ItemID);

        URL url = new URL(builder.build().toString());
        return url;
    }




}


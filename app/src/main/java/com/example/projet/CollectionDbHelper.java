package com.example.projet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;
//import static androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.Table.map;

public class CollectionDbHelper extends SQLiteOpenHelper {

    private static final String TAG = CollectionDbHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "collection.db";

    public static final String TABLE_NAME = "collection";

    public static final String _ID = "_id";
    public static final String COLUMN_ITEM_NAME = "item";
    public static final String COLUMN_ITEM_ID = "idItem";

    public static final String COLUMN_CATEGORIES_NAME = "categories";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TIME_FRAME = "timeFrame";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_TECHNICAL_DETAILS = "technicalDetails";
    public static final String COLUMN_WORKING = "working";
    public static final String COLUMN_PICTURES = "pictures";

    public CollectionDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_ITEM_NAME + " TEXT NOT NULL, " +
                COLUMN_ITEM_ID + " TEXT, " +
                COLUMN_CATEGORIES_NAME + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_TIME_FRAME + " TEXT, " +
                COLUMN_YEAR + " INTEGER, " +
                COLUMN_BRAND + " TEXT, " +
                COLUMN_TECHNICAL_DETAILS + " INTEGER, " +
                COLUMN_WORKING + " BOOLEAN, " +
                COLUMN_PICTURES + " TEXT, " +

                // To assure the application have just one team entry per
                // team name and league, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ITEM_NAME +  ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_BOOK_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private ContentValues fill(Item item) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ITEM_NAME, item.getName());
        values.put(COLUMN_ITEM_ID, item.getId());
        values.put(COLUMN_CATEGORIES_NAME, item.getCategories2());       //fusion en 1 string
        values.put(COLUMN_DESCRIPTION, item.getDescription());
        values.put(COLUMN_TIME_FRAME, item.getTimeFrame2());             //fusion en 1 string
        values.put(COLUMN_YEAR, item.getYear());
        values.put(COLUMN_BRAND, item.getBrand());
        values.put(COLUMN_TECHNICAL_DETAILS, item.getTechnicalDetails2());  //fusion en 1 string
        values.put(COLUMN_WORKING, item.isWorking());
        values.put(COLUMN_PICTURES, item.getPicture2());                 //fusion en 1 string

        return values;
    }

    public boolean addTeam(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(item);

        Log.d(TAG, "adding: "+item.getName()+" with id="+item.getId());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (team, championship)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    public int updateTeam(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(item);

        // updating row
        return db.updateWithOnConflict(TABLE_NAME, values, _ID + " = ?",
                new String[] { String.valueOf(item.getId()) }, CONFLICT_IGNORE);
    }


    public Cursor fetchAllTeams(int pos,String searchString) {         //rajouter argument pour ordre chronologique et changer la requette en fonction
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        if(pos ==0)
        {
            cursor = db.query(TABLE_NAME, null,
                    null, null, null, null, COLUMN_ITEM_NAME +" ASC", null);
        }
        else if(pos==3)
        {

           /* String query = "SELECT * FROM collection WHERE item=%s";
            String q = String.format(query, "\""+searchString + "%\"");
            cursor= db.rawQuery(q, null);*/
            final String whereClause = COLUMN_ITEM_NAME + " like ?";
            String[] whereArgs = new String[]{"%" + searchString + "%"};

            cursor = db.query(TABLE_NAME,
                    null, // columns
                    whereClause, // selection
                    whereArgs, // selectionArgs
                    null, // groupBy
                    null, // having
                    null, // orderBy
                    null); // limit

        }
        else
        {
            cursor = db.query(TABLE_NAME, null,
                    null, null, null, null, COLUMN_ITEM_NAME +" DESC", null);
        }


        Log.d(TAG, "call fetchAllTeams()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }


    public List<Item> getAllTeams() {
        // TODO

        List<Item> res = new ArrayList<>();
        Cursor cursor = fetchAllTeams(0,"");
        while(cursor.moveToNext())
        {

            res.add(cursorToItem(cursor));
        }
        cursor.close();
        return res;
    }

    public void deleteTeam(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public void populate() {

        JSONResponseHandlerItemList list = new JSONResponseHandlerItemList();
        JSONResponseHandlerItem item = new JSONResponseHandlerItem();
        List<String> array;
        Item it;
        try {
            URL url = new URL("https://demo-lia.univ-avignon.fr/cerimuseum/ids")/*WebServiceUrl.buildSearchTeamList()*/;
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();



            InputStream in = new BufferedInputStream(connection.getInputStream());
            array = list.readJsonStream(in);
            connection.disconnect();
            for(int i=0;i<array.size();i++) {
                Log.d("aaaaaaaaaaaaaaaaa", array.get(i));
                String temp = "https://demo-lia.univ-avignon.fr/cerimuseum/items/" + array.get(i);
                url = new URL(temp);//WebServiceUrl.buildSearchTeam(temp);
                connection = (HttpURLConnection) url.openConnection();
                in = new BufferedInputStream(connection.getInputStream());
                item.readJsonStream(in);
                it = item.getItem();
                it.setId(array.get(i));
                //Log.d("aaabbb", it.getBrand());
                //Log.d("aaabbb", it.getCategories2());
                //Log.d("aaabbb", it.getDescription());
                //Log.d("aaabbb", it.getName());
               addTeam(it);
                connection.disconnect();

            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.d("aaaaaaaaaaaaaaaaa", "wresha");
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("aaaaaaaaaaaaaaaaa", "wreshb");
        }



    }

    public Item cursorToItem(Cursor cursor) {

        Log.d("aaaaa", cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORIES_NAME)));
        Item item = new Item(cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_NAME)),
                Stream.of(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORIES_NAME)).split("///")).map(String::trim).collect(Collectors.toList()),
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                Stream.of(cursor.getString(cursor.getColumnIndex(COLUMN_TIME_FRAME)).split("///")).map(String::trim).collect(Collectors.toList()),
                cursor.getInt(cursor.getColumnIndex(COLUMN_YEAR)),
                cursor.getString(cursor.getColumnIndex(COLUMN_BRAND)),
                Stream.of(cursor.getString(cursor.getColumnIndex(COLUMN_TECHNICAL_DETAILS)).split("///")).map(String::trim).collect(Collectors.toList()),
                cursor.getInt(cursor.getColumnIndex(COLUMN_WORKING)),
                Stream.of(cursor.getString(cursor.getColumnIndex(COLUMN_PICTURES)).split("///")).map(String::trim).collect(Collectors.toList())
        );

        return item;
    }
}


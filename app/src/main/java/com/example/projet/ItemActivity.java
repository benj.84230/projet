package com.example.projet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class ItemActivity extends AppCompatActivity {
    private TextView textItemName,textCategories,textDescription,textTimeFrame,textYear,textBrand,textTechnicalDetail,textWorking,textPictures;
    private ImageView imageview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);


        Item item = (Item) getIntent().getParcelableExtra("Item");

        textItemName = (TextView) findViewById(R.id.nameItem);
        textCategories = (TextView) findViewById(R.id.editCategories);
        textDescription = (TextView) findViewById(R.id.editdescription);
        textTimeFrame = (TextView) findViewById(R.id.edittimeFrame);

        textYear = (TextView) findViewById(R.id.edityear);
        textBrand = (TextView) findViewById(R.id.editbrand);
        textTechnicalDetail = (TextView) findViewById(R.id.edittechnicalDetails);
        textWorking = (TextView) findViewById(R.id.editworking);
        textPictures = (TextView) findViewById(R.id.editPictures);
        imageview = (ImageView) findViewById(R.id.imageView);

        textItemName.setText(item.getName());
        textCategories.setText(item.getCategories2());
        textDescription.setText(item.getDescription());
        textTimeFrame.setText(item.getTimeFrame2());

        textYear.setText(String.valueOf(item.getYear()));
        textBrand.setText(item.getBrand());
        textTechnicalDetail.setText(item.getTechnicalDetails2());
        textWorking.setText(String.valueOf(item.isWorking()));
        textPictures.setText(item.getPicture2());

        String temp = "https://demo-lia.univ-avignon.fr/cerimuseum/items/" + item.getId() + "/thumbnail";
        URL url = null;
        try {
            url = new URL(temp);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageview.setImageBitmap(bmp);
    }
}

package com.example.projet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Spinner spinner;
    Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        final CollectionDbHelper db = new CollectionDbHelper(this);
        //db.populate();
        cursor = db.fetchAllTeams(0,"");
        final ListView vue = (ListView) findViewById(R.id.listview);
        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.listview, cursor, new String[] {db.COLUMN_ITEM_NAME, db.COLUMN_BRAND },  new int[] { R.id.list_row_title,R.id.list_row_content });
        vue.setAdapter(adapter);

        spinner = (Spinner) findViewById(R.id.spinner);
        //Création d'une liste d'élément à mettre dans le Spinner(pour l'exemple)
        List exempleList = new ArrayList();
        exempleList.add("ordre alphabétique");
        exempleList.add("ordre chronologique");
        exempleList.add("Affichage par catégories");

        /*Le Spinner a besoin d'un adapter pour sa presentation alors on lui passe le context(this) et
                un fichier de presentation par défaut( android.R.layout.simple_spinner_item)
        Avec la liste des elements (exemple) */
        ArrayAdapter adapter2 = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                exempleList
        );

        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Enfin on passe l'adapter au Spinner et c'est tout
        spinner.setAdapter(adapter2);

        Button button = (Button)findViewById(R.id.button);
        EditText text = (EditText)findViewById(R.id.text_input);
        //listener sur bouton
        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                cursor=db.fetchAllTeams(3,text.getText().toString());
                Log.d("mamama",text.getText().toString());
                adapter.changeCursor(cursor);

                adapter.notifyDataSetChanged();
            }
        });
        vue.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                //Cursor cursor= db.fetchAllTeams(0);
                cursor.moveToFirst();
                Item temp = null;
                if (cursor.getPosition() == pos) {
                    temp = db.cursorToItem(cursor);
                }
                while (cursor.moveToNext()) {
                    if (cursor.getPosition() == pos) {
                        temp = db.cursorToItem(cursor);
                    }
                }

                Intent ItemActivity = new Intent(getApplicationContext(), com.example.projet.ItemActivity.class);
                ItemActivity.putExtra("Item",temp);
                adapter.notifyDataSetChanged();
                startActivity(ItemActivity);

            }

        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("azazazazaz", String.valueOf(position));
                if(position==1)
                {
                    cursor=db.fetchAllTeams(1,"");
                    adapter.changeCursor(cursor);

                    adapter.notifyDataSetChanged();
                }

                if(position==0)
                {
                    cursor=db.fetchAllTeams(0,"");
                    adapter.changeCursor(cursor);

                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}

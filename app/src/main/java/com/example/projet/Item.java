package com.example.projet;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Item implements Parcelable {

    public static final String TAG = Item.class.getSimpleName();
    private String id; // used for the _id colum of the db helper

    private String name;
    private List<String> categories = new ArrayList<String>();
    private String description;
    private List<String> timeFrame = new ArrayList<String>();

    private int year;
    private String brand;
    private List<String> technicalDetails = new ArrayList<String>();
    private int working;
    private List<String> picture = new ArrayList<String>();

    public Item(){}

    public Item(String id, String name, List<String> categories, String description, List<String> timeFrame) {
        this.id = id;
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
    }

    public Item(String id, String name, List<String> categories, String description, List<String> timeFrame, int year, String brand, List<String> technicalDetails, int working, List<String> picture)
    {
        this.id = id;
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;

        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.working = working;
        this.picture = picture;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //to do !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        dest.writeString(id);
        dest.writeString(name);
        dest.writeList(categories);
        dest.writeString(description);
        dest.writeList(timeFrame);
        dest.writeInt(year);
        dest.writeString(brand);
        dest.writeList(technicalDetails);
        dest.writeInt(working);
        dest.writeList(picture);
    }

    public Item(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.categories = new ArrayList<String>();
        in.readList(this.categories,String.class.getClassLoader());
        this.description = in.readString();
        this.timeFrame = new ArrayList<String>();
        in.readList(this.timeFrame,String.class.getClassLoader());
        this.year = in.readInt();
        this.brand = in.readString();
        this.technicalDetails = new ArrayList<String>();
        in.readList(this.technicalDetails,String.class.getClassLoader());
        this.working = in.readInt();
        this.picture = new ArrayList<String>();
        in.readList(this.picture,String.class.getClassLoader());
    }

    public static final Creator<Item> CREATOR = new Creator<Item>()
    {
        @Override
        public Item createFromParcel(Parcel source)
        {
            return new Item(source);
        }

        @Override
        public Item[] newArray(int size)
        {
            return new Item[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCategories() {
        return categories;
    }

    public String getCategories2() {

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < categories.size(); i++) {
            stringBuilder.append(categories.get(i));
            if(i<categories.size()-1)
            {
                stringBuilder.append("///");
            }

        }
        return stringBuilder.toString();
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getTimeFrame() {
        return timeFrame;
    }

    public String getTimeFrame2()
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < timeFrame.size(); i++) {
            stringBuilder.append(timeFrame.get(i));
            if(i<timeFrame.size()-1)
            {
                stringBuilder.append("///");
            }

        }
        return stringBuilder.toString();
    }

    public void setTimeFrame(List<String> timeFrame) {
        this.timeFrame = timeFrame;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<String> getTechnicalDetails() {
        return technicalDetails;
    }

    public String getTechnicalDetails2() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < technicalDetails.size(); i++) {
            stringBuilder.append(technicalDetails.get(i));
            if(i<technicalDetails.size()-1)
            {
                stringBuilder.append("///");
            }

        }
        return stringBuilder.toString();
    }

    public void setTechnicalDetails(List<String> technicalDetails) {
        this.technicalDetails = technicalDetails;
    }


    public int isWorking() {
        return working;
    }

    public void setWorking(int working) {
        this.working = working;
    }

    public List<String> getPicture() {
        return picture;
    }

    public String getPicture2() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < picture.size(); i++) {
            stringBuilder.append(picture.get(i));
            if(i<picture.size()-1)
            {
                stringBuilder.append("///");
            }

        }
        return stringBuilder.toString();
    }

    public void setPicture(List<String> picture) {
        this.picture = picture;
    }
}
